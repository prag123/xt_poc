window.onload = function () {
  let productData = getShoppingBagItems();
  let total_items = `${productData.length} items`;

  let shopping_bag_template = `<tr>
          <th colspan="2">${total_items}</th>
          <th>size</th>
          <th>qty</th>
          <th>price</th>
      </tr>`;

  for (let element in productData) {

    shopping_bag_template += ` <tr>
    <td class="productImg"><img src="${productData[element]['image']}" /></td>
    <td>
        <p class="productTitle">${productData[element]['description']}</p>
        <p>Style #: ${productData[element]['style']}</p>
        <p>Color: ${productData[element]['color']}</p>
        <div class="productControls">
            <a href="">Edit</a> | <a href="#"> X REmove </a> | <a href="#">save for later</a>
        </div>
    </td>
    <td>
        <p class="productSize">${productData[element]['size']}</p>
    </td>
    <td><input value=" ${productData[element]['qty']}" class="productQty"/></td>
    <td>
        <p class="productPrice"><sup>$</sup>${productData[element]['price']}</p>
    </td>
</tr>`
  };

  $("#shoppingBagItems").html(shopping_bag_template);
  $("#item_no").html(total_items);
}